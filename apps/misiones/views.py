from django.shortcuts import render
from apps.misiones.forms import ImageUploadForm
from django.http import HttpResponseForbidden
from django.http import HttpResponse
from apps.misiones.models import Misiones

def MisionesView(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            idmision = request.POST.get('idmision')
            m = Misiones.objects.get(id_mision=idmision)
            m.imagen_mision = form.cleaned_data['image']
            m.estado_mision = "En Proceso"
            m.save()
            errors = 'Imagen Enviada con Exito'
            context = {              
                'errors': errors
            }
            return render(request, 'misiones/misiones.html', context)
        else:
            errors = 'Imagen No Valida'
            context = {     
                'errors': errors
            }
        return render(request, 'misiones/misiones.html', context)