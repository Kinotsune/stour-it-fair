from django.db import models
from apps.usuarios.models import Usuarios
from apps.lugares.models import Lugares

class Misiones(models.Model):
    id_mision = models.AutoField(primary_key=True)    
    descripcion_mision = models.CharField(max_length=100)
    cant_estrellas = models.IntegerField()
    fecha_hora_limite = models.DateTimeField()
    estado_mision = models.CharField(max_length=50,default='Pendiente')
    imagen_mision = models.ImageField(upload_to='apps/misiones/static/misiones/misiones',default='image.png')
    rut_usuario = models.ForeignKey(Usuarios,on_delete=models.CASCADE,null=True)
