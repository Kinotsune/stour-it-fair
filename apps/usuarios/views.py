from django.shortcuts import render, redirect
from apps.misiones.models import Misiones
from apps.usuarios.models import Usuarios
from apps.premios.models import Premios
from apps.lugares.models import Lugares
from django.contrib import messages

from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import logout as django_logout,login as do_login
from django.views.generic import View, ListView
from apps.usuarios.forms import UsuarioForm,LoginForm,PerfilForm,CambioPerfilForm,RecuperarForm

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required

def home(request):
    return render(request, 'usuarios/home.html')

def perfil(request):
    return render(request, 'usuarios/perfil.html')

def recuperar(request):
    return render(request, 'usuarios/recuperar.html')    
  
def premios(request):
    datos3 = Premios.objects.all().order_by('cant_estrellas')
    return render(request, 'premios/premios.html',{ "datos3": datos3}) 

def ranking(request):
    datos2 = Usuarios.objects.all().order_by('-cant_lugares')[:20]
    return render(request, 'usuarios/Ranking.html',{ "datos2": datos2})     

def misiones(request):
    datos = Misiones.objects.filter(rut_usuario=request.user.rut_usuario)[:30]
    return render(request, 'misiones/misiones.html',{ "datos": datos})

def lugarespendientes(request):
    datos4 = Lugares.objects.filter(rut_usuario=request.user.rut_usuario)[:30]
    return render(request, 'lugares/lugarespendientes.html',{ "datos4": datos4})

def lugaresrecorridos(request):
    return render(request, 'lugares/lugaresrecorridos.html')   

def modificar(request):
    return render(request, 'usuarios/modificar.html')       

def login(request):
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                do_login(request, user)  
                return redirect('home.html')
    return render(request, "usuarios/login.html", {'form': form})   

class LogoutView(View):
    def get(self, request):
        if request.user.is_authenticated:
            django_logout(request)
        return redirect('login')

class RegisterUserView(View):

    def get(self, request):
        form = UsuarioForm()
        context = {
            'reg_form': form,
            'success_message': ''
        }
        return render(request, 'usuarios/registro.html', context)

    def post(self, request):
        errors = ''
        user_clien = Usuarios()
        form = UsuarioForm(request.POST,request.FILES, instance=user_clien)
        if form.is_valid():
            form_u = form
            new_user = form.save()
            errors = 'Usuario guardado con éxito!, desea iniciar sesion?'
            form = LoginForm()
            context = {
                'reg_form': form,
                'errors': errors
            }
            return render(request, 'usuarios/home.html', context)
        else:
            errors = 'Informacion no valida'
            context = {
                'reg_form': form,
                'errors': errors
            }
            return render(request, 'usuarios/registro.html', context)

class MyProfileView(View):
    @method_decorator(login_required())
    def get(self, request):
        form = UsuarioForm()
        errors = ''
        possible_users = Usuarios.objects.filter(username=request.user.username)
        if len(possible_users)!=0:
            user = possible_users[0]
            form = PerfilForm(instance=user)
        context = {
            'form': form,
            'errors': errors
        }
        return render(request, 'usuarios/perfil.html', context)

    def post(self,request):
        pass

class CambioPerfilView(View):
    #@method_decorator(login_required())
    def get(self, request):
        form = CambioPerfilForm()
        errors = ''
        context = {
            'form': form,
            'errors': errors
        }
        return render(request, 'usuarios/modificar.html', context)

    #@method_decorator(login_required())
    def post(self, request):
        errors = ''
        form = CambioPerfilForm(request.POST)
        if form.is_valid():
            pwd1 = form.cleaned_data.get('pwd1', '')
            pwd2 = form.cleaned_data.get('pwd2', '')
            possible_user = Usuarios.objects.filter(username=request.user.username)
            user = possible_user[0]
            user.set_password(pwd2)
            user.save()
            errors = 'Datos Modificados con Exito'

            form = LoginForm()
            context = {
                'login_form': form,
                'errors': errors
            }
            template = 'usuarios/modificar.html'
        else:
            form = CambioPerfilForm()
            errors = 'Error formulario no valido!!!'
            template = 'usuarios/modificar.html'
            context = {
                'form': form,
                'errors': errors
            }
            
        return render(request, template, context)

class RecuperarView(View):
    def get(self, request):
        form = RecuperarForm()
        errors = ''
        context = {
            'form': form,
            'errors': errors
        }
        return render(request, 'usuarios/recuperar.html', context)

    def post(self, request):
        errors = ''
        form = RecuperarForm(request.POST)
        if form.is_valid():
            rut2 = form.cleaned_data.get('rut', '')
            usr2 = form.cleaned_data.get('usr', '')
            email2 = form.cleaned_data.get('email', '')
            pwd1 = form.cleaned_data.get('pwd1', '')
            possible_user = Usuarios.objects.filter(username=usr2,rut_usuario=rut2,email=email2)
            user = possible_user[0]
            user.set_password(pwd1)
            user.save()
            errors = 'Datos Modificados con Exito'
            form = RecuperarForm()
            context = {
                'form': form,
                'errors': errors
            }
            template = 'usuarios/recuperar.html'
        else:
            form = RecuperarForm()
            errors = 'Error Datos Incorrectos'
            template = 'usuarios/recuperar.html'
            context = {
                'form': form,
                'errors': errors
            }   
        return render(request, template, context)