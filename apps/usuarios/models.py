from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuarios(AbstractUser):
    rut_usuario = models.CharField(max_length=10, primary_key=True,blank=True,unique=True)
    cant_lugares = models.IntegerField(default=0,blank=True)
    cant_estrellas = models.IntegerField(default=0,blank=True)
    imagen_usuario = models.ImageField(upload_to='apps/usuarios/static/usuarios/usuarios',default='image.png')
