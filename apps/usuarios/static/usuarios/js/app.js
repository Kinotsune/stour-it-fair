$(document).ready(function(){
    $("#login").validate({
        errorClass:"is-invalid",
        rules:{
            nombreusuario:{
                required:true
            },
            contraseña:{
                required: true,
            }
        },
        messages:{
            nombreusuario:{
                required:"Debe ingresar su nombre de usuario"
            },
            contraseña:{
                required:"Debe ingresar su contraseña"
            }
        }
    })
})

$(document).ready(function(){
    $("#registro").validate({
        errorClass:"is-invalid",
        rules:{
            nombre:{
                required:true
            },
            apellido:{
                required:true
            },
            nombreusuario:{
                required:true
            },
            edad:{
                required:true
            },
            correoelectronico:{
                required:true,
                email : true
            },
            contraseña:{
                required: true
            },
            mismacontraseña:{
                required: true
            }
        },
        messages:{
            nombre:{
                required:"Debe ingresar su Nombre"
            },
            apellido:{
                required:"Debe ingresar su Apellido"
            },
            nombreusuario:{
                required:"Debe ingresar su nombre de usuario"
            },
            edad:{
                required:"Debe ingresar su edad"
            },
            correoelectronico:{
                required:"Debe ingresar su Correo Electronico"
            },
            contraseña:{
                required: "Debe ingresar una contraseña"
            },
            mismacontraseña:{
                required: "Debe repetir la contraseña colocada anteriormente"
            }
        }
    })
})

$(document).ready(function(){
    $("#perfil").validate({
        errorClass:"is-invalid",
        rules:{
            nombre:{
                required:true
            },
            apellido:{
                required:true
            },
            nombreusuario:{
                required:true
            },
            correoelectronico:{
                required:true,
                email : true
            },
            nuevacontraseña:{
                required: true
            },
            reiterarcontraseña:{
                required: true
            }
        },
        messages:{
            nombre:{
                required:"Debe ingresar su nuevo Nombre"
            },
            apellido:{
                required:"Debe ingresar su nuevo Apellido"
            },
            nombreusuario:{
                required:"Debe ingresar su nuevo nombre de usuario"
            },
            correoelectronico:{
                required:"Debe ingresar su nuevo Correo Electronico"
            },
            nuevacontraseña:{
                required: "Debe ingresar una nueva contraseña"
            },
            reiterarcontraseña:{
                required: "Debe repetir la contraseña colocada anteriormente"
            }
        }
    })
})

$("#login").submit(function(){
    if($("#login").valid()){
        return true
    }else{
        Swal.fire({
            type: 'Error de Registro',
            title: 'Problemas?',
            text: 'Algo anda mal!!!!',
            footer: '<a href>Porque yo tengo este problema?</a>'
          })
    }
    return false
})

$("#registro").submit(function(){
    if($("#registro").valid()){
        Swal.fire({
            type: 'success',
            title: 'Exito',
            text: 'Te has registrado correctamente en Stour',
            footer: '<a href>Disfruta</a>'
          })
    return true
    }
    else{
        Swal.fire({
            type: 'error',
            title: 'Que problema?',
            text: 'Falta algun dato del formulario',
            footer: '<a href>Porque yo tengo este problema?</a>'
          })
    }
    return false
})

$("#perfil").submit(function(){
    if($("#perfil").valid()){
        Swal.fire({
            type: 'success',
            title: 'Exito',
            text: 'Has editado correctamente tus datos',
            footer: '<a href>Mucho Exito</a>'
          })
    return true
    }
    else{
        Swal.fire({
            type: 'error',
            title: 'Que problema?',
            text: 'Falta algun dato del formulario',
            footer: '<a href>Porque deseas modificar tus datos?</a>'
          })
    }
    return false
})
