from django import forms
from django.core.exceptions import ValidationError

from apps.usuarios.models import Usuarios


class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuarios
        exclude = ['first_name','last_name','is_active','is_staff','is_superuser','last_login','date_joined','groups','user_permissions']
        fields = (
            'username','email','rut_usuario','password','imagen_usuario',)
        labels = {
            'username': 'Nombre de usuario', 
            'email': 'Correo electronico',
            'rut_usuario' : 'Rut Usuario',
            'password': 'Contraseña',
            'imagen_usuario' : 'Foto de Perfil',
             }
        
        help_texts = { 'password': ' ', 'username': ' '}

        widgets = {
            'username': forms.TextInput(attrs={'class':'input-material label-material'}),
            'email': forms.EmailInput(attrs={'class':'input-material'}),
            'rut_usuario': forms.TextInput(attrs={'class':'input-material'}),
            'password': forms.PasswordInput(attrs={'class':'input-material'}),
            'imagen_usuario': forms.FileInput(attrs={'class':'input-material'}),      
        }
    
    def clean_username(self):
        cleaned_data = super(UsuarioForm, self).clean()
        username = cleaned_data.get('username', '').lower()
        users = Usuarios.objects.filter(username=username)
        if len(users) == 0:
            return username
        else:
            raise ValidationError('Error el nombre de usuario '+str(username)+' ya esta registrado!!')

    def clean_email(self):
        cleaned_data = super(UsuarioForm, self).clean()
        email = cleaned_data.get('email', '').lower()
        users = Usuarios.objects.filter(email=email)
        if len(users) == 0:
            return email
        else:
            raise ValidationError('Error el correo electronico '+str(email)+' ya esta registrado!!')

    def clean_rut(self):
        cleaned_data = super(UsuarioForm, self).clean()
        rut_usuario = cleaned_data.get('rut_usuario', '').lower()
        users = Usuarios.objects.filter(rut_usuario=rut_usuario)
        if len(users) == 0:
            return rut_usuario
        else:
            raise ValidationError('Error el usuario con rut '+str(rut_usuario)+' ya esta registrado!!')
        
    def save(self, commit=True): 
        user = super(UsuarioForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class LoginForm(forms.Form):
    rut = forms.CharField(label='Nnombre de Usuario')
    pwd = forms.CharField(label='Contraseña', widget=forms.PasswordInput())

class PerfilForm(forms.ModelForm):
    class Meta:
        model = Usuarios
        exclude = ['first_name','last_name','is_active','is_staff','is_superuser','last_login','date_joined','groups','user_permissions','rut']
        fields = (
            'username','email', 'password',)
        labels = {
            'username': 'Nombre de usuario', 
            'email': 'Correo electronico',
            'password': 'Contraseña',
             }
        
        help_texts = { 'password': ' ', 'username': ' '}

        widgets = {
            'password': forms.PasswordInput(),      
        }
    def save(self, commit=True): 
        user = super(PerfilForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class CambioPerfilForm(forms.Form):
    pwd2 = forms.CharField(label='Nueva Contraseña' , widget=forms.PasswordInput(
                                    attrs={
                                        'class':'input-material',      
                                                                     
                                    }))
    pwd3 = forms.CharField(label='Confirmar Contraseña', widget=forms.PasswordInput(
                                    attrs={
                                        'class':'input-material',
                            
                                    }))
    pwd1 = forms.CharField(label='Contraseña Actual', widget=forms.PasswordInput(
                                    attrs={
                                        'class':'input-material',
                                        'id':'con-act'
                                        
                                    }))                                

    def clean_pwd3(self):
        cleaned_data = super(CambioPerfilForm, self).clean()
        pwd2 = cleaned_data.get('pwd2', '')
        pwd3 = cleaned_data.get('pwd3', '')
        if pwd2 == pwd3:
            return pwd3
        else:
            raise ValidationError('Error las contraseñas no son iguales!!')        

class RecuperarForm(forms.Form):
    rut = forms.CharField(label='Rut' , widget=forms.TextInput(
                                    attrs={
                                        'class':'input-material',      
                                                                     
                                    }))
    usr = forms.CharField(label='Nombre de usuario', widget=forms.TextInput(
                                    attrs={
                                        'class':'input-material',
                            
                                    }))
    email = forms.CharField(label='Correo electronico', widget=forms.TextInput(
                                    attrs={
                                        'class':'input-material',
                                        
                                        
                                    }))
    pwd1 = forms.CharField(label='Nueva Contraseña', widget=forms.PasswordInput(
                                    attrs={
                                        'class':'input-material',
                                        
                                        
                                    }))                     
    '''
    def clean_username(self):
        cleaned_data = super(RecuperarForm, self).clean()
        username = cleaned_data.get('usr', '').lower()
        users = Usuarios.objects.filter(username=username)
        if len(users) == 0:
            return username
        else:
            raise ValidationError('Error el nombre de usuario '+str(username)+' ya esta registrado!!')

    def clean_email(self):
        cleaned_data = super(RecuperarForm, self).clean()
        email = cleaned_data.get('email', '').lower()
        users = Usuarios.objects.filter(email=email)
        if len(users) == 0:
            return email
        else:
            raise ValidationError('Error el correo electronico '+str(email)+' ya esta registrado!!')

    def clean_rut(self):
        cleaned_data = super(RecuperarForm, self).clean()
        rut = cleaned_data.get('rut', '').lower()
        users = Usuarios.objects.filter(rut_usuario=rut)
        if len(users) == 0:
            return rut
        else:
            raise ValidationError('Error el rut '+str(rut)+' ya esta registrado!!')        
    '''
    def clean_comprobacion(self):
        cleaned_data = super(RecuperarForm, self).clean()
        rut2 = cleaned_data.get('rut', '')
        usr2 = cleaned_data.get('usr', '')
        email2 = cleaned_data.get('email', '')
        users = Usuarios.objects.filter(rut_usuario=rut2,username=usr2,email=email2)
        if users.rut_usuario == rut2 and users.username == usr2 and users.email == email2:
            return rut2
        else:
            raise ValidationError('Error Los datos no son correctos')      
