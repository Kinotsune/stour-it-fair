from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import login
from django.urls import path
from .views import home,misiones,ranking,premios,recuperar,lugarespendientes,perfil,lugaresrecorridos,login,modificar
from .views import LogoutView,RegisterUserView,MyProfileView,CambioPerfilView,RecuperarView
from apps.lugares.views import LugaresView
from apps.misiones.views import MisionesView

urlpatterns = [
    path('', login, name = "login"),
    path('home.html/', home, name="home"),
    url(r'^registro/$', RegisterUserView.as_view(), name='registro' ),
    url(r'^logout/$', LogoutView.as_view(), name='logout' ),
    path('perfil.html/', perfil, name = "perfil"),
    url(r'^modificar/$', CambioPerfilView.as_view(), name='modificar' ),
    url(r'^misiones/$', MisionesView, name='misiones2' ),
    path('Misiones.html/', misiones, name = "misiones"), 
    url(r'^recuperar/$', RecuperarView.as_view(), name='recuperar' ),  
    path('ranking.html/', ranking, name = "ranking"),
    path('premios.html/', premios, name = "premios"),
    url(r'^lugarespendientes/$', LugaresView, name='lugarespendientes2' ),
    path('lugarespendientes.html/', lugarespendientes, name = "lugarespendientes"),
    path('lugaresrecorridos.html/', lugaresrecorridos, name = "lugaresrecorridos"),
    
   
]
