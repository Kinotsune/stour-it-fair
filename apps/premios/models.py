from django.db import models

class Premios(models.Model):
    id_premio = models.AutoField(primary_key=True)
    nombre_premio = models.CharField(max_length=100)
    cant_estrellas = models.IntegerField()
    imagen_premio = models.ImageField(upload_to='apps/premios/static/premios/premios')
