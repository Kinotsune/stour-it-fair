# Generated by Django 2.2.6 on 2019-11-23 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('premios', '0002_auto_20191122_2010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='premios',
            name='imagen_premio',
            field=models.ImageField(upload_to='apps/premios/static/premios/premios2'),
        ),
    ]
