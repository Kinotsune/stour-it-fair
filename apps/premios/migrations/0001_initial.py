# Generated by Django 2.2.6 on 2019-11-22 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Premios',
            fields=[
                ('id_premio', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_premio', models.CharField(max_length=100)),
                ('cant_estrellas', models.IntegerField()),
                ('imagen_premio', models.ImageField(upload_to='')),
            ],
        ),
    ]
