from django.db import models
from apps.usuarios.models import Usuarios

class Lugares(models.Model):
    id_lugar = models.AutoField(primary_key=True)
    nombre_lugar = models.CharField(max_length=50)
    rut_usuario = models.ForeignKey(Usuarios,on_delete=models.CASCADE,null=True) 
    estado_lugar = models.CharField(max_length=100,default='Pendiente')
    imagen_lugar = models.ImageField(upload_to='apps/lugares/static/lugares/lugares',default='image.png')

