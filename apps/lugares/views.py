from django.shortcuts import render
from apps.lugares.forms import ImageUploadForm
from django.http import HttpResponseForbidden
from django.http import HttpResponse
from apps.lugares.models import Lugares

def LugaresView(request):
    if request.method == 'POST':
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            idlugar = request.POST.get('idlugar')
            m = Lugares.objects.get(id_lugar=idlugar)
            m.imagen_lugar = form.cleaned_data['image']
            m.estado_lugar = "En Proceso"
            m.save()
            errors = 'Imagen Enviada con Exito'
            context = {
                
                'errors': errors
            }
            return render(request, 'lugares/lugarespendientes.html', context)
        else:
            errors = 'Imagen No Valida'
            context = {
                
                'errors': errors
            }
        return render(request, 'lugares/lugarespendientes.html', context)